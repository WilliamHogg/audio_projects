var scribble = require('scribbletune');

var clip1 = scribble.clip({
  notes: 'c4 e4 f4',
  pattern: 'x_-'.repeat(12),
  noteLength:'1/32'
});

var clip2 = scribble.clip({
  notes: 'c4 e4 f4',
  pattern: 'x_xxx_x_',
  noteLength:'1/32'
})

scribble.midi(clip1.concat(clip2, 'music.mid'));
